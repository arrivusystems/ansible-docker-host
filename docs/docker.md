# run shipyard docker registry

sudo docker run -i -t -p 443 -v /var/docker/registry/ssl:/opt/ssl -e SSL_CERT_PATH=/opt/ssl/jigsawacademy.csr -e SSL_CERT_KEY_PATH=/opt/ssl/jigsawacademy.key shipyard/docker-private-registry

# run docker registry with beacon ssl keys

sudo docker run -i -t -p 443 -v /var/docker/registry/ssl:/opt/ssl -e SSL_CERT_PATH=/opt/ssl/jigsawacademy.csr -e SSL_CERT_KEY_PATH=/opt/ssl/jigsawacademy.key shipyard/docker-private-registry --name docker-registry


# to build the arrivu/passenger-customizable image 
make build_customizable NAME=arrivu/passenger

# to push the arrivu/passenger-customizable image to docker public repo
sudo docker push arrivu/passenger-customizable

 # to build the datastore container
 sudo docker build --rm=true -t "arrivu/canvas-redis-datastore" .

# start docker registry
 sudo docker run -p 5000:5000 -d registry --name -v /var/docker/registry/conf:/registry-conf -e DOCKER_REGISTRY_CONFIG=/registry-conf/config.yml -e CORS_ORIGINS=\'*\' odk211/docker-registry

 # start the docker registry ui 

 sudo docker run -p 8080:8080 -d --name registry_ui --link registry:registry -e REG1=http://localhost:5000/v1/ atcol/docker-registry-ui


 sudo docker run -d -v /var/docker/mysql:/var/lib/mysql tutum/mysql /bin/bash -c "/usr/bin/mysql_install_db"

# Docker registry

sudo docker run \
		 -v /var/docker/registry/conf:/registry-conf \
		 -e DOCKER_REGISTRY_CONFIG=/registry-conf/config.yml
         -e SETTINGS_FLAVOR=s3 \
         -e AWS_BUCKET=acme-docker \
         -e STORAGE_PATH=/registry \
         -e AWS_KEY=AKIAHSHB43HS3J92MXZ \
         -e AWS_SECRET=xdDowwlK7TJajV1Y7EoOZrmuPEJlHYcNP2k4j49T \
         -e SEARCH_BACKEND=sqlalchemy \         
         -p 5000:5000 \
         registry:0.7.3

# start the docker registry

sudo docker run -i -t -d -p 5000:5000 --name="arrivu-docker-registry" -v /var/docker/registry/conf:/registry-conf -e DOCKER_REGISTRY_CONFIG=/registry-conf/config.yml registry:0.7.3

# registry_web_data data only container
sudo docker run -v /var/docker/registry/ui/h2 --name="registry_web_data" ubuntu

# registry web ui with data only container
sudo docker run  -i -t -d -p 8080:8080 --name="arrivu-docker-registry-ui" --volumes-from=registry_web_data atcol/docker-registry-ui


sudo docker run -i -t -d -p 443 --name="arrivu-docker-registry" -v /var/docker/registry/ssl/:/opt/ssl -e SSL_CERT_PATH=/opt/ssl/jigsawacademy.csr -e SSL_CERT_KEY_PATH=/opt/ssl/jigsawacademy.key shipyard/docker-private-registry


# Start nsenter in a machine via docker container

docker run --rm -v /usr/local/bin:/target jpetazzo/nsenter


# run google cAdvisor

sudo docker run -i -t -d \
  --volume=/var/run:/var/run:rw \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --publish=8080:8080 \
  --detach=true \
  --name=cadvisor \
  google/cadvisor:latest

  # open port 5000 in iptables for docker registry
  sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT

===============================================================================

# postgresql

sudo docker run -d --name postgresql \
                  -v /data/postgresql:/data \
                  -p 127.0.0.1:5432:5432 \
                  -e USER="dbadmin" \
                  -e PASS="dbadmin123$" \
                  -e DB="lms_production lms_queue_production badge_production" \
                  arrivu/postgresql

docker run -t -i --name psql --link postgresql:db ubuntu bash

       psql -U "$DB_ENV_USER" \
       -h "$DB_PORT_5432_TCP_ADDR" \
       -p "$DB_PORT_5432_TCP_PORT" \
       lms_production


sudo docker commit <containeer_id_of_psql> arrivu/psql

sudo docker run -d --name canvas_datastore -p 6380:6379 arrivu/canvas-redis-datastore

sudo docker run -d --name canvas_cache -p 6381:6379 arrivu/canvas-redis-cache

sudo docker run -t -i --name canvas_lms \
                    -v /data/lms \
                    --link postgresql:db \
                    --link canvas_datastore:datastore \
                    --link canvas_cache:cache \
                    arrivu/passenger-customizable:0.9.11 bash


------------------------------------------------------------------------------------


sudo docker run -d  -v /var/log/postgresql:/var/log/postgresql \
                --name pglog \
                arrivu/postgresql echo Data-only container for postgres log

sudo docker run -d -v /var/docker/postgresql/data:/data \
                --name pgdata \
                arrivu/postgresql echo Data-only container for postgres data            


sudo docker run -d --volumes-from pgdata \
                  --volumes-from pglog \
                  --name postgresql \
                  -p 127.0.0.1:5432:5432 \
                  -e USER="dbadmin" \
                  -e PASS="dbadmin123$" \
                  -e DB="lms_production lms_queue_production badge_production" \
                  arrivu/postgresql


sudo docker run -d --name canvas_datastore -p 6380:6379 arrivu/canvas-redis-datastore


sudo docker run -d --name canvas_cache -p 6381:6379 arrivu/canvas-redis-cache


sudo docker build --rm=true -t "arrivu/canvas-base" .

sudo docker run -i -t arrivu/passenger-customizable:0.9.11 /bin/bash