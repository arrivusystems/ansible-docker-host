# TO run a playbook

ansible-playbook -u sysadmin -s base-common.yml -vvvv -K

ansible-playbook -u sysadmin -s base-security.yml -vvvv -K

ansible-playbook -u sysadmin -s base-firewall.yml -vvvv -K -e 'ansible_ssh_port=1001'

ansible-playbook -u sysadmin -s base-docker.yml -vvvv -K -e 'ansible_ssh_port=1001'
